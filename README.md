# gulpfileの始め方

## config.jsonの設定をまずはする

#### root：
*  projectDir・・・プロジェクトが置いてあるディレクトリ
*  uploadDir・・・ftp/sftpでアップロードのベースとなるディレクトリ
*  changeDir・・・ファイルの差分を検知するためのディレクトリ

#### browser：
*  root・・・ローカル環境の立ち上げる際のロート

#### project：
各タスクを実行する際のディレクトリ / ファイル名設定

*  ディレクトリ・・・root.projectDirの中のディレクトリを設定する
*  integrateFile / configfile・・・・ファイル名のみ設定

例）sass
```
base.projectDir + project.sasssrcDir　//・・・コンパイル前ファイル
base.projectDir + project.sasssrcDir　//・・・コンパイル後ファイル
```

#### taskswitch：
*  img・・・画像圧縮
*  sass・・・sassコンパイル
*  jsmin・・・jsの圧縮
*  jsintegrate・・・jsの統合（1ファイルにまとめる）
*  sftp・・・上記タスクの後、SFTPでサーバーにアップする
*  ftp・・・上記タスクの後、FTPでサーバーにアップする
*  localHost・・・ローカル環境もgulpで立ち上げる

#### sassStyle：
*  sassexp・・・コンパイルしたデータはminifyしない
*  sassmin・・・コンパイルしたデータはminifyする
*  sassCompass・・・compassを使ってコンパイルする

#### jsStyle：
*  jses6・・・es6の変換をする


## gulpを実際に動かしてみる
#### 前提：
node.jsをインストールする必要あり（→v12.16.1で動作確認済み）

#### 実行コマンド：
```
cd "gulpfileのあるディレクトリ"
npm install //・・・必要なパッケージをダウンロード！！
npx gulp  //・・・タスクランナーの実行！！
```

#### ファイル初期値の構造
```
├- gulpfile/
  ├-- gulpfile.js
  ├-- config.json
  ├-- package.json
  ├-- package-lock.json
  └-- tasks/...
├- html/
  ├-- sass/[コンパイル前ファイル]
  ├-- css/[コンパイル後ファイル](gulpで生成)
  ├-- images/
      ├-- [圧縮後ファイル](gulpで生成)
      ├-- ...
      └-- src/
          ├-- [圧縮前ファイル]
          └-- ...
  └-- js/
      ├-- [圧縮後ファイル](gulpで生成)
      ├-- ...
      ├-- integrate/
          ├-- [統合前ファイル]
          └-- ...
      ├-- src/
          ├-- [圧縮前ファイル]
          ├-- integrate.js(gulpで生成)
          └-- ...
      └-- strict/
          ├-- [ES6変換後ファイル](gulpで生成)
          └-- ...
└- changedfiles/[最新変更ファイル](gulpで生成・差分を検知するため)
```

#### compass利用の場合のファイル構造
```
├-- gulpfile.js
├-- config.json
├-- package.json
├-- package-lock.json
├-- tasks/...
├- html/
  ├-- ...
```
（gulpfileでまとめちゃうとパスの関係でうまく行かない...）

## 注意事項 
**compassの動作は保証できない...!!!**そもそもgulpを導入している人でcompassを使う人があんまりいない？？記事出てこない...最低限、とりあえず動くようにしている感じです。（どうしてもcompass派の人はsassFunc.jsを編集してください。。）

そして、「脱Compass」的な記事が2〜3年前から結構出ていたので、参考までに


[compassからlibsassへ。gulpの設定を見直して大幅速度UP！](https://www.okushin.co.jp/kodanuki_note/2017/10/compass%E3%81%8B%E3%82%89libsass%E3%81%B8%E3%80%82gulp%E3%81%AE%E8%A8%AD%E5%AE%9A%E3%82%92%E8%A6%8B%E7%9B%B4%E3%81%97%E3%81%A6%E5%A4%A7%E5%B9%85%E9%80%9F%E5%BA%A6up%EF%BC%81.html)
